// keywords => here we can identify the certain keywords taht describe the collection of our package.
// GOAL: Create a server using Express.js
// 1. User the require() diretive to acquire the express library and it's utilities.
	const express = require('express');

// 2. Create a server using only express
	const server = express();
		// express() => will allow us to create an express application

// 3. Identify a port/address that will serve/host this newly created connection/app.
	const address = 3000;

// 4. Bind the applciation to the desired designated port using the listen(). Create a method that will
// display a response that the connection has been established.
	server.listen(address, () => {
		console.log(`Server is running on port: ${address}`);
	});